# Fullstack developer (MEAN)

## Explicación

El test consiste en crear un formulario de registro (Signup) de usuarios. Como información personal se debe contemplar:

1. Nombre
2. Apellidos
3. RUT *
4. Email
5. Región **
6. Comuna **
7. Dirección
8. Fecha de nacimiento
9. Foto de perfil

```
* http://cauditor2.blogspot.com/2009/09/modulo-11-o-validador-de-rut.html`
```
```
** https://www.bcn.cl/siit/nuestropais/regiones
```

## Requerimientos

1. Se deben validar los campos según correspondan. **Todos los campos son requeridos, salvo la foto de perfil del usuario**.

2. Si el usuario es menor a 18 años, debe mostrar una alerta y no permitir el registro.
3. Si el usuario que se registra no es de la Región Metropolitana, mostrar alerta y permitir registro.
4. Limitar el tamaño máximo de la foto de perfil que sube el usuario a 2MB.
5. Validar si existe email y rut, asíncronamente luego (o a medida) que el usuario ingresa los datos.

## Requisitos técnicos de la solución

1. Framework NodeJS (ganas puntos si utilizas ExpressJS) para crear microservicio.
2. Libertad para escoger cualquier herramienta frontend.
3. Persistencia de datos (pierdes puntos si NO utilizas MongoDB).
4. Usar docker para levantar la app (Totalmente requerido).
5. Si le agregas testing (front/back) ganas puntos extras.
6. Breve README.md con explicación de la solución y herramientas utilizadas.

## Formato de entrega

Se debe enviar un pull request (PR) a este repo.

El PR debe incluir una shell que permita levantar los contenedores, setear la config y poblar con 
data para visualizar el proyecto correctamente. Indicar en qué puerto corre la app.